//
//  NSDate+ISO8601String.m
//  TWToolkit
//
//  Created by Sam Soffes on 5/26/10.
//  Copyright 2010 Tasteful Works, Inc. All rights reserved.
//

#import "NSDate+ISO8601String.h"
#import "NSString+containsString.h"

@implementation NSDate (ISO8601String)

- (NSString *)ISO8601UTCString {
	NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSLocale *usLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
    [dateFormatter setLocale:usLocale];
	return [dateFormatter stringFromDate:self];
}


- (NSString *)ISO8601String {
	NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSLocale *usLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
    [dateFormatter setLocale:usLocale];
	return [dateFormatter stringFromDate:self];
}

- (NSString *)dateISO8601String {
	NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
    [dateFormatter setLocale:usLocale];
	return [dateFormatter stringFromDate:self];
}

+ (NSDate *) currentDate{
	NSDateFormatter *dateFmter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFmter setTimeStyle:NSDateFormatterNoStyle];
	[dateFmter setDateStyle:NSDateFormatterMediumStyle];
	
	NSString* dateText = [ dateFmter stringFromDate: [NSDate date] ]; // !! !! I need dateText anyway
	
	return [ dateFmter dateFromString: dateText ]; // !! truncate time to 00:00:00	
	
}

@end
