//
//  TWHUDView.h
//  TWToolkit
//
//  Created by Sam Soffes on 9/29/09.
//  Copyright 2009 Tasteful Works, Inc. All rights reserved.
//

#import "TWHUDView.h"
#import "UIImage+BundleImage.h"
#import <QuartzCore/QuartzCore.h>

static CGFloat kHUDSize = 172.0;
static CGFloat kIndicatorSize = 40.0;

@interface TWHUDView(private)
-(void)setHudFrame;
@end

@implementation TWHUDView

@synthesize textLabel = _textLabel;
@synthesize activityIndicator = _activityIndicator;
@synthesize loading = _loading;

#pragma mark NSObject

- (id)init {
	return [self initWithTitle:nil loading:YES];
}


- (void)dealloc {
	[_activityIndicator release];
	[_textLabel release];
	[super dealloc];
}

#pragma mark UIView

- (id)initWithFrame:(CGRect)frame {
	return [self initWithTitle:nil loading:YES];
}


- (void)drawRect:(CGRect)rect {

	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// Draw rounded rectangle
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.5);
	CGRect rrect = CGRectMake(0.0, 0.0, kHUDSize, kHUDSize);
	CGFloat radius = 14.0;
	CGFloat minx = CGRectGetMinX(rrect);
	CGFloat midx = CGRectGetMidX(rrect);
	CGFloat maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect);
	CGFloat midy = CGRectGetMidY(rrect);
	CGFloat maxy = CGRectGetMaxY(rrect);	
	CGContextMoveToPoint(context, minx, midy);
	CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
	CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
	CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
	CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
	CGContextClosePath(context);
	CGContextFillPath(context);
	
	// Checkmark
	if (_loading == NO) {
		UIImage *checkmark = [UIImage imageNamed:@"images/hud-checkmark.png" bundle:@"TWToolkit.bundle"];
		[checkmark drawInRect:CGRectMake(round((kHUDSize - 36.0) / 2.0), round((kHUDSize - 40.0) / 2.0), 36.0, 40.0)];
	}
}



- (void)layoutSubviews {
    

    _activityIndicator.frame = CGRectMake(round((kHUDSize - kIndicatorSize) / 2.0), round((kHUDSize - kIndicatorSize) / 2.0), kIndicatorSize, kIndicatorSize);
	_textLabel.frame = CGRectMake(0.0, round(kHUDSize - 30.0), kHUDSize, 20.0);
	
	NSLog(@"layoutSubviews - frame: %@", NSStringFromCGRect(self.frame));

}

#pragma mark UIAlertView

- (void)show {

    [super show];
	self.bounds = CGRectMake(0,0, kHUDSize, kHUDSize);
}


// Deprecated. Overridding UIAlertView's setTitle.
- (void)setTitle:(NSString *)aTitle {
	_textLabel.text = aTitle;
}

#pragma mark HUD

- (id)initWithTitle:(NSString *)aTitle {
	return [self initWithTitle:aTitle loading:YES];
}


- (id)initWithTitle:(NSString *)aTitle loading:(BOOL)isLoading {
    
  	if ((self = [super initWithFrame:CGRectZero])) {
        
        [self setHudFrame];

		// Indicator
		_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		_activityIndicator.alpha = 0.0;
		[_activityIndicator startAnimating];
		[self addSubview:_activityIndicator];
		
		// Text Label
		_textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		_textLabel.font = [UIFont boldSystemFontOfSize:14];
		_textLabel.backgroundColor = [UIColor clearColor];
		_textLabel.textColor = [UIColor whiteColor];
		_textLabel.shadowColor = [UIColor blackColor];
		_textLabel.shadowOffset = CGSizeMake(0.0, 1.0);
		_textLabel.textAlignment = NSTextAlignmentCenter;
		_textLabel.lineBreakMode = UILineBreakModeTailTruncation;
		_textLabel.text = aTitle ? aTitle : @"Loading";
		[self addSubview:_textLabel];
		
		// Loading
		self.loading = isLoading;
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orientationChanged:)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object:nil];

	}
	return self;
}

-(void)setHudFrame{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if(!UIDeviceOrientationIsLandscape(deviceOrientation) && !UIDeviceOrientationIsPortrait(deviceOrientation)){
        //we don't care if the device is just placed down.  Ignore those
        return;
    }
    
    CGRect bounds=[UIScreen mainScreen].applicationFrame; 
    if(UIDeviceOrientationIsLandscape(deviceOrientation)){
        double t=bounds.size.width;
        bounds.size.width=bounds.size.height;
        bounds.size.height=t;
    }
    self.frame = CGRectMake(bounds.size.width/2 - kHUDSize/2, bounds.size.height/2-kHUDSize/2, kHUDSize, kHUDSize);
 
}

- (void)orientationChanged:(NSNotification *)notification
{
    [self setHudFrame];
}



- (void)completeWithTitle:(NSString *)aTitle {
	self.loading = NO;
	_textLabel.text = aTitle;
}


- (void)completeAndDismissWithTitle:(NSString *)aTitle {
	[self completeWithTitle:aTitle];
	[self performSelector:@selector(dismiss) withObject:nil afterDelay:1.0];
}


- (void)dismiss {
	[self dismissAnimated:YES];
}


- (void)dismissAnimated:(BOOL)animated {
	[super dismissWithClickedButtonIndex:0 animated:animated];
}

#pragma mark Setters

- (void)setLoading:(BOOL)isLoading {
	_loading = isLoading;
	_activityIndicator.alpha = _loading ? 1.0 : 0.0;
	[self setNeedsDisplay];
	
}

@end
