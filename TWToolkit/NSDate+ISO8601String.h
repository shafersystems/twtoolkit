//
//  NSDate+ISO8601String.h
//  TWToolkit
//
//  Created by Sam Soffes on 5/26/10.
//  Copyright 2010 Tasteful Works, Inc. All rights reserved.
//

@interface NSDate (ISO8601String)

- (NSString *)ISO8601String;
- (NSString *)ISO8601UTCString;
- (NSString *)dateISO8601String;

+ (NSDate *)currentDate;

@end
