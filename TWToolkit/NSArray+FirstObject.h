//
//  NSArray+FirstObject.h
//  TWToolkit
//
//  Created by Joel Shafer on 1/4/11.
//  Copyright 2011 Tasteful Works, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSArray (FirstObject)

- (id)firstObject;

@end
