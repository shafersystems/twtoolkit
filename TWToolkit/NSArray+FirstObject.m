//
//  NSArray+FirstObject.m
//  TWToolkit
//
//  Created by Joel Shafer on 1/4/11.
//  Copyright 2011 Tasteful Works, Inc. All rights reserved.
//

#import "NSArray+FirstObject.h"

@implementation NSArray (FirstObject)

- (id)firstObject
{
    if ([self count] > 0)
    {
        return [self objectAtIndex:0];
    }
    return nil;
}

@end
